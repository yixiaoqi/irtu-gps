package io.nutz.irtu.gpsserver.bean;

public abstract class AbstractDevMsg {
    protected String imei;
    protected long srvtime;
    public String getImei() {
        return imei;
    }
    public void setImei(String imei) {
        this.imei = imei;
    }
    public long getSrvtime() {
        return srvtime;
    }
    public void setSrvtime(long srvtime) {
        this.srvtime = srvtime;
    }
    
    
}
